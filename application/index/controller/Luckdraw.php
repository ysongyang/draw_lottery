<?php
/**
 * 摇奖控制器
 * User: ysongyang
 * Date: 2018/8/12
 * Time: 20:02
 */

namespace app\index\controller;

use think\Controller;
use think\Db;
use think\Session;

class Luckdraw extends Controller
{

    public $user_id = 0;
    public $lot_rate = [];
    public $user = array();

    public function _initialize()
    {
        parent::_initialize();
        $this->user = self::usersInit();
        $this->user_id = $this->user['user_id'];
        Session::set('user', $this->user);
        $this->assign('user', $this->user); //存储用户信息
        $lot_rate = db('config')->where("inc_type = 'lot_rate'")->column('name,value');
        $this->lot_rate = $lot_rate;
        $this->assign('lot_rate', $lot_rate); //存储用户信息
    }


    public function index()
    {
        $record_line = db('lot_log')->order('addtime desc')->limit(50)->select();
        $this->assign('record_line', $record_line);
        return $this->fetch();
    }

    /**
     * 获取大转盘中奖率
     */
    public function getPrizerate()
    {
        //var zjl=[0,0,0,2,16.5,81.5,21];
        echo 'var zjl=[' . $this->lot_rate["cj_one_jiang_rate"] . ',' . $this->lot_rate["cj_two_jiang_rate"] . ',' . $this->lot_rate["cj_three_jiang_rate"] . ',' . $this->lot_rate["cj_four_jiang_rate"] . ',' . $this->lot_rate["cj_five_jiang_rate"] . ',' . $this->lot_rate["cj_six_jiang_rate"] . '];';

    }

    /**
     * 获取大转盘奖项内容
     */
    public function getPrizeinfo()
    {
        //[ "一等奖一万元", "二等奖3000元", "三等奖2000元", "四等奖1000元", "五等奖","六等奖"];
        echo 'var restaraunts=["一等奖\n' . $this->lot_rate['cj_one_jiang'] . '", "二等奖\n' . $this->lot_rate['cj_two_jiang'] . '", "三等奖\n' . $this->lot_rate['cj_three_jiang'] . '", "四等奖\n' . $this->lot_rate['cj_four_jiang'] . '", "五等奖\n' . $this->lot_rate['cj_five_jiang'] . '","六等奖\n' . $this->lot_rate['cj_six_jiang'] . '"];';

    }


    public function getPrizetext()
    {
        $html = '<div class="jiang" style="width:64%;text-align: left;margin-left:18%">一等奖:<span>' . $this->lot_rate['cj_one_jiang'] . '</span></br>二等奖:<span>' . $this->lot_rate['cj_two_jiang'] . '</span></br>三等奖:<span>' . $this->lot_rate['cj_three_jiang'] . '</span></br>四等奖:<span>' . $this->lot_rate['cj_four_jiang'] . '</span></br>五等奖:<span>' . $this->lot_rate['cj_five_jiang'] . '</span></br>六等奖:<span>' . $this->lot_rate['cj_six_jiang'] . '</span></div>';
        echo $html;
    }


    /**
     * 抽奖的处理方法
     * 添加抽奖记录
     * 扣除会员积分
     * 抽奖次数减一
     */
    public function addPrize()
    {
        if ($this->request->isPost()) {
            $cj_min_num = $this->lot_rate['cj_min_num']; //获取抽奖每次所用的积分
            Db::startTrans();
            $data['rank'] = input('post.rank');
            $data['price'] = input('post.prize');
            $data['user_id'] = $this->user_id;
            $data['username'] = $this->user['username'];
            $data['addtime'] = time();
            $data['state'] = 1;
            $result = db('lot_log')->insert($data);
            if ($result) {
                #处理剩余奖品数量
                $where['name'] = 'cj_shengyu';
                $where['inc_type'] = 'lot_rate';
                db('config')->where($where)->setDec('value');
                Db::commit();
                $json = ['code' => 0, 'msg' => $data['price']];
                echo json_encode($json);
                die;
            } else {
                Db::rollback();
                $json = ['code' => 1, 'msg' => '系统错误，请刷新后再试'];
                echo json_encode($json);
                die;
            }
        }
    }


    public function logs($page = 1)
    {
        $where['user_id'] = $this->user_id;
        $list = db('lot_log')->where($where)->order('addtime desc')->paginate(10, false, ['page' => $page]);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 初始化用户
     * 这里联操作数据库，请跟进自己情况修改
     */
    protected static function usersInit()
    {
        return [
            'user_id' => 1,  //uid
            'points' => 100000, //积分
            'username' => '小A', //用户昵称
        ];
    }
}