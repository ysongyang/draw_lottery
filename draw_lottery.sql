/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50639
Source Host           : localhost:3306
Source Database       : draw_lottery

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2018-08-12 20:46:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_config
-- ----------------------------
DROP TABLE IF EXISTS `tp_config`;
CREATE TABLE `tp_config` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `value` varchar(512) DEFAULT NULL,
  `inc_type` varchar(20) DEFAULT NULL,
  `desc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_config
-- ----------------------------
INSERT INTO `tp_config` VALUES ('105', 'cj_min_num', '3000', 'lot_rate', '抽奖所需积分');
INSERT INTO `tp_config` VALUES ('106', 'cj_shengyu', '15', 'lot_rate', '活动抽奖总次数');
INSERT INTO `tp_config` VALUES ('107', 'cj_one_jiang', '10000元', 'lot_rate', '1等奖');
INSERT INTO `tp_config` VALUES ('108', 'cj_two_jiang', '3000元', 'lot_rate', '2等奖');
INSERT INTO `tp_config` VALUES ('109', 'cj_three_jiang', '2000元', 'lot_rate', '3等奖');
INSERT INTO `tp_config` VALUES ('110', 'cj_four_jiang', '1000元', 'lot_rate', '4等奖');
INSERT INTO `tp_config` VALUES ('111', 'cj_five_jiang', '200元', 'lot_rate', '5等奖');
INSERT INTO `tp_config` VALUES ('112', 'cj_six_jiang', '5L油一桶', 'lot_rate', '6等奖');
INSERT INTO `tp_config` VALUES ('113', 'cj_one_jiang_rate', '0', 'lot_rate', '1等奖中奖几率');
INSERT INTO `tp_config` VALUES ('114', 'cj_two_jiang_rate', '0', 'lot_rate', '2等奖中奖几率');
INSERT INTO `tp_config` VALUES ('115', 'cj_three_jiang_rate', '0', 'lot_rate', '3等奖中奖几率');
INSERT INTO `tp_config` VALUES ('116', 'cj_four_jiang_rate', '2', 'lot_rate', '4等奖中奖几率');
INSERT INTO `tp_config` VALUES ('117', 'cj_five_jiang_rate', '16.5', 'lot_rate', '5等奖中奖几率');
INSERT INTO `tp_config` VALUES ('118', 'cj_six_jiang_rate', '81.5', 'lot_rate', '6等奖中奖几率');

-- ----------------------------
-- Table structure for tp_lot_log
-- ----------------------------
DROP TABLE IF EXISTS `tp_lot_log`;
CREATE TABLE `tp_lot_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `price` varchar(10) DEFAULT NULL COMMENT '奖品名称',
  `rank` int(11) DEFAULT '0' COMMENT '中间等级',
  `addtime` int(11) DEFAULT '0' COMMENT '添加时间',
  `state` tinyint(3) DEFAULT '0' COMMENT '1兑换 2作废',
  `redeemtime` int(11) DEFAULT '0' COMMENT '兑奖时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=180 DEFAULT CHARSET=utf8 COMMENT='抽奖记录';

-- ----------------------------
-- Records of tp_lot_log
-- ----------------------------
INSERT INTO `tp_lot_log` VALUES ('174', '1', '小A', '六等奖5L油一桶', '6', '1534076629', '1', '0');
INSERT INTO `tp_lot_log` VALUES ('175', '1', '小A', '六等奖5L油一桶', '6', '1534077311', '1', '0');
INSERT INTO `tp_lot_log` VALUES ('176', '1', '小A', '六等奖5L油一桶', '6', '1534077390', '1', '0');
INSERT INTO `tp_lot_log` VALUES ('177', '1', '小A', '六等奖5L油一桶', '6', '1534077754', '1', '0');
INSERT INTO `tp_lot_log` VALUES ('178', '1', '小A', '六等奖5L油一桶', '6', '1534077854', '1', '0');
INSERT INTO `tp_lot_log` VALUES ('179', '1', '小A', '五等奖200元', '5', '1534077947', '1', '0');
